﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public class MongoConfigurationOptions
    {
        public string Connection { get; set; }
        public string DatabaseName { get; set; }
        public string EmployeeCollectionName { get; set; }
        public string RoleCollectionName { get; set; }

    }
}
