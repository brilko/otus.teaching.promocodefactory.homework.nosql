﻿using MongoDB.Driver;
using System.Collections.Generic;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer
         : IDbInitializer
    {
        private readonly IMongoCollection<Employee> employeeCollection;
        private readonly IMongoCollection<Role> roleCollections;

        public MongoDbInitializer( 
            IMongoCollection<Employee> employeeCollection, IMongoCollection<Role> roleCollections)
        {
            this.employeeCollection = employeeCollection;
            this.roleCollections = roleCollections;
        }

        private async void DeleteCreateInitializeCollection<T>(
            IMongoCollection<T> collection, IEnumerable<T> initializeData) 
        {
            await collection.DeleteManyAsync(_ => true);
            await collection.InsertManyAsync(initializeData);
        }

        public void DeleteCreateInitializeDb()
        {
            DeleteCreateInitializeCollection(employeeCollection, FakeDataFactory.Employees);
            DeleteCreateInitializeCollection(roleCollections, FakeDataFactory.Roles);
        }
    }
}
