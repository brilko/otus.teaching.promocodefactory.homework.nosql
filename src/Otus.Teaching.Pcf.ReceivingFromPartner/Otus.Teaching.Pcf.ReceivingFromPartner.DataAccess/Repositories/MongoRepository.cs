﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoCollection<T> collection;
        public MongoRepository(IMongoCollection<T> collection)
        {
            this.collection = collection;
        }

        public async Task AddAsync(T entity)
        {
            await collection.InsertOneAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await collection.DeleteOneAsync(item => item.Id == entity.Id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var cursor = await collection.FindAsync(_ => true);
            return cursor.ToList();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var cursor = await collection.FindAsync(item => item.Id == id);
            return cursor.FirstOrDefault();
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            var cursor = await collection.FindAsync<T>(predicate);
            return cursor.FirstOrDefault();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var cursor = await collection.FindAsync(item => ids.Contains(item.Id));
            return cursor.ToList();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            var cursor = await collection.FindAsync<T>(predicate);
            return cursor.ToList();
        }

        public async Task UpdateAsync(T entity)
        {
            await collection.ReplaceOneAsync(item => item.Id == entity.Id, entity);
        }
    }
}
